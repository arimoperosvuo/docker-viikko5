// Viikkotehtävä 3 Perosvuo Arimo 0438125, 2018
// Lista
"use strict"

let appsi = null;

//Ladataan javascript vasta dokumentin latautumisen jälkeen
document.addEventListener("DOMContentLoaded", function(event) {
	//Alustetaan vue. Luodaan sinne datarakenne "lista" (joka on lista).
	appsi = new Vue({
		el: "#lista",
		data: {
			lista: [
			],
			uusiAlkio: null,
		},
		//Vuen perusfunktio. Suoritetaan ennen metodeja
		mounted() {
			if (localStorage.getItem("lista")) {
			  try {
			    this.lista = JSON.parse(localStorage.getItem("lista"));
				} catch(e){
			    localStorage.removeItem("lista");
				}
			}
		},
		//Itse määritellyt funktiot. Lisätään listaan, poistetaan listasta, tallennetaan lista localiin.
		methods: {
			addJuttu: function(uusiAlkio) {
				try {
					if(uusiAlkio == null) throw "on tyhjä!";
					if(uusiAlkio.length > 20) throw "on liian pitkä (>20).";
					if(uusiAlkio.match(/[^a-öA-Ö0-9 ,.]/) !== null) throw "sisältää erikoismerkkejä (ei saa sisältää)!";
					this.lista.push({alkio:uusiAlkio});
				}
				catch(error) {
					window.alert("Syöte " + error);
				}
			},
			poistaJuttu: function(index) {
				this.lista.splice(index,1);
			},
			saveJuttu() {
				const parsed = JSON.stringify(this.lista);
				localStorage.setItem("lista", parsed);
			},
			poistaKaikki: function(){
				this.lista = [];
			}
		}
	})
});