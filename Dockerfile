FROM nginx
COPY default.conf /etc/nginx/conf.d/default.conf
COPY index.html /usr/share/nginx/html/index.html
COPY javascript.js /usr/share/nginx/html/javascript.js
EXPOSE 80